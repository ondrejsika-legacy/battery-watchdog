#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "battery-watchdog",
    version = "1.0.1",
    url = 'https://github.com/ondrejsika/battery-watchdog',
    license = 'MIT',
    description = "Battery watchdog primarily for i3wm via notify-send",
    author = 'Ondrej Sika',
    author_email = 'ondrejsika@ondrejsika.com',
    scripts = ("battery-watchdog", ),
    install_requires = ("acpi"),
    include_package_data = True,
)