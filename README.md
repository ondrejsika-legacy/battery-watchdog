Battery watchdog
================

Battery watchdog primarily for i3wm via notify-send

__Author__: Ondrej Sika, <http://ondrejsika.com>, <ondrej@ondrejsika.com>

__GitHub__: <https://github.com/ondrejsika/python-acpi>

__PyPI__: <http://pypi.python.org/pypi/acpi>


Documentation
-------------

### Installation

```
pip install battery-watchdog
```

### Usage

Notifi if battery level is lower than 20%.

```
battery-watchdog -l 20
```

Notifi if remaining time is lower than 30 min.

```
battery-watchdog -t 1800  # 30 min
```

and add to crontab

```
echo "* * * * * sika export DISPLAY=:0.0; battery-watchdog -l 30" >> /etc/crontab
service cron restart
```

Changeslog
----------

### v1.0.1

don't send notification if acpi status is Charging
